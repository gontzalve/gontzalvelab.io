[Atrás](index.html)

<h2>Presentes invisibles</h2>



*qué tan completa puede ser la felicidad*

*sin el pesimismo adulto,*

*sin la consciencia de la suerte y de la muerte,*

*sin el contraste de un mundo de mierda.*



*quiero aprender a disfrutar*

*el silencio de mi mejor sonrisa,*

*el sosiego de una mente que apaga sus luces y enciende el corazón,*

*al menos durante una imagen fugaz,* 

*en la que a pesar de todo*

*el mundo deja de ser tan gris.*