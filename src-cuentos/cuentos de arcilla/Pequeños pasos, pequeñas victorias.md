[Atrás](index.html)

<h2>Pequeños pasos, pequeñas victorias</h2>

Desde hace ya un par de semanas, la pequeña Inés se había dado cuenta de algo que llamó mucho su atención. Todos los días, incluidos los días en los que debería estar descansando, su papá llegaba del trabajo, se lavaba las manos con el detergente de turno y se sentaba a esperar la cena, siempre en el mismo asiento de la mesa de la cocina. 

Inés esperó unos días para ver si sólo se trataba de una coincidencia, pero luego de semana y media confirmó el patrón. Confirmó, también, que todos lo sabían: ése era el asiento de papá. Lo supo porque al día siguiente de notar el patrón, ella ocupó ese asiento antes que él llegara. Ni bien su hermano mayor entró a la cocina, un tanto confundido la cargó y la sentó en la siguiente silla. 

—Ahí se sienta el viejo —le dijo. 

La manera en la que lo dijo no dio lugar a preguntas, no porque haya sido un comentario cortante o agresivo, sino por la naturalidad y el convencimiento en su explicación. El ejercicio de esa fe hacia las respuestas que le daban no le resultaba ajeno, pues habían muchas cosas en la vida que ella aún no entendía por su corta edad, y su mamá y su hermano mayor eran los encargados de resolverle los porqués que se le iban presentando.

Sabía que los días de cielo gris se daban porque ella no se portaba bien, que los días de lluvia eran la tristeza del mundo al recordar que su abuelito ya no está y que, ahora entendía, cada persona tiene un asiento fijo en la mesa. Comprendió rápido, también, que en realidad no podía hacer nada para poner a prueba alguna de esas convenciones de la vida, salvo sentarse en el asiento de papá.

Lo intentó una y otra vez: a veces su mamá la corría, otras veces su hermano, sorprendido por aquellas reincidencias rebeldes. No servía pedir, no servía llorar, no servía sujetarse al asiento con todas sus fuerzas.

Y es que no había lugar para negociaciones en la dinámica de esta casa, especialmente cuando el ritmo se incrementaba diez minutos antes de las seis de la tarde. Ya estaba todo calculado: el hervor del agua, el calor del arroz y el doblez de las servilletas. A las seis en punto, la reja del jardín se oía y, acto seguido, el ritmo único de los pasos de papá. Era el inicio de otra etapa en la convivencia, tranquilamente tensa por el constante pero silencioso escrutinio paterno, con el ceño fruncido, como una suerte de paz armada.

Y en la mesa a su hermano siempre se le olvidaba ponerle algo al viejo. O el cuchillo para la carne, la cucharita para el café o el ají que sabía que le iba a pedir. Inés no tenía idea de que su hermano lo hacía por joder al viejo, porque le molestaba que fuera tan servido, tan acostumbrado a que le hagan todo.

—Al menos me lo vas a pedir —pensaba.

Pero el día de hoy fue diferente. Su mamá había recibido una visita y la terminó acompañando al paradero con tal de no cortar la conversación. En un arranque de anarquismo preescolar, la pequeña Inés aprovechó la soledad de la cocina post-lonche y se sentó a esperar, vigilante, en el asiento de papá.

Veinte minutos infinitos la pusieron a dormir sobre la mesa y a su hermano mayor el corazón no le dejó despertarla cuando fue a la cocina a alistar la cena.

Cuando ambos papás llegaron a casa, la escena de la cocina le arrancó una sonrisa a mamá, y se acercó para despertarla y moverla de sitio. 

—No la despiertes —le dijo el papá con voz suave, pero con tono firme—. Déjala dormir. 

La respuesta sorprendió tanto a madre e hijo. El papá fue a lavarse las manos y al regresar a la cocina, se las secó con el mantel que no debía y se sentó en el banquito del costado. Con una sonrisa nueva en su diccionario, recogió el cabello que le caía en el rostro a Inés y se lo puso tras la pequeña oreja. 

Acto seguido, el viejo buscó con la mirada la cucharita del café y, frunciendo el ceño, empezó a comer.

—La cucharita —dijo al aire luego de una breve pausa, a pesar de que el cajón de los cubiertos estaba a su costado. 



​																									*Para Rocío, por todo lo que merecías y no tuviste.*

