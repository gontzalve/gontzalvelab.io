[Atrás](index.html)

<h2>Pausa</h2>



*como los rayos de sol que sobreviven a un día nublado*

*o como una avecilla que confía en mi hombro,*

*mi mente me hace el frágil presente del silencio,*

*un tratado de paz con la eterna lucha interna,*

*una pausa en la batalla de los miedos y preocupaciones,*

*el regalo del ahora.*



*Son espacios en donde no existen líneas de tiempo,*

*ni pasados ni futuros,*

*ni reproches ni planes para mañana,*

*ni deudas o promesas.*

*Solo estamos yo*

*y la vida.*