[Atrás](index.html)

<h2>Recordatorios violentos</h2>

Con los audífonos puestos para evitar escuchar las groserías que algunas personas le decían, Elena caminaba por la avenida Paseo Colón con rumbo al cuarto que alquilaba. Las baladas la ponían más sensible de lo que ella quería, así que contrarrestaba los cláxones e insultos de una Lima en hora punta a ritmo de salsa y bachata. 

Vestía unos jeans clásicos y una blusa blanca que dejaba sus hombros descubiertos. En su modesto guardarropa, había cambiado los tacos por unas zapatillas que la ayuden a pasar desapercibida, especialmente frente a aquel bar por el que sí o sí tenía que pasar para llegar a su cuarto, en una estrecha calle de Breña. A pocos metros de ser en realidad un callejón, la calle de su cuarto era de las pocas que contaba con un alumbrado público funcional, por lo que cualquier desvío de la ruta principal era demasiado arriesgado para su seguridad.

Sin embargo, el simple hecho de caminar frente a ese bar resultaba un ejercicio emocional para el cual debía prepararse incluso desde antes de salir del trabajo. Había aprendido a lidiar con todo tipo de comentarios desde pequeña, pero nunca en su vida había encontrado a alguien que le provoque tanto miedo como *Toro*, uno de los borrachines de aquel bar de mala muerte.

*Toro* era un tipo rechoncho y de fácil carcajada, fiel cliente del bar desde que su mujer lo dejó. De tez clara, el color de su nariz era el termómetro de una embriaguez que empezaba religiosamente a las seis de la tarde. 

Reunido todos los días con el mismo grupo de borrachos, todos amigos suyos, *Toro* lideraba espectáculos de silbidos y piropos sucios a cuanta mujer pase por esa estrecha calle. Muchas de ellas, endurecidas por el acoso diario, le contestaban e insultaban con las llaves entre los dedos. Generalmente las cosas no escalaban a más, quedando todo en palabras y con *Toro* enrojeciéndose por la risa.

Pero con Elena era diferente: la carcajada barata era reemplazada por una expresión inequívoca de odio y, los piropos subidos de tono, por insultos y amenazas. Como habiendo enviado una involuntaria declaración de guerra, Elena aparecía todos los días en aquella calle caminando en la acera del frente, escondiendo el miedo y la ansiedad en un ceño fruncido y un paso firme.

El día de hoy, Elena llegaba a la calle de su cuarto más tarde de lo habitual. Había hecho un par de paradas en unas tiendas de ropa, y esperaba con emoción poder probarse cada prenda en la confianza de su cuarto. 

La emoción rápidamente se convirtió en extrañeza cuando, al entrar a la callejuela del bar, vio al mismo grupo de borrachos anónimos y notó que la silla desde donde *Toro* dominaba la calle estaba vacía. Lejos de relajarse, puso en pausa la música y apuró el paso. Manteniéndose atenta a su alrededor, buscó a punta de tacto las llaves dentro de la cartera. 

Cuando por fin llegó al umbral de su puerta, llaves en mano y un poco más tranquila, bajó la guardia y no se percató de que *Toro* cruzaba la pista desde la acera opuesta y en dirección a ella.

Sin darle tiempo a reaccionar, una mano empujó su cabeza hacia adelante, haciéndola golpear fuertemente contra la puerta de madera. Las bolsas y la cartera abierta cayeron al piso, esparciendo sus contenidos por toda la acera. A pesar de la conmoción, Elena logró mantenerse de pie y atinó a correr, alejándose de *Toro*. 

—¡Ven acá cabro de mierda! —gritó *Toro* mientras la perseguía.

Bastante mareada, Elena no pudo sostener el equilibrio por mucho tiempo y terminó tropezando un par de casas después. Aprovechando eso, *Toro* se acercó y empezó a forcejear con ella, quien daba golpes al aire, defendiéndose. En el forcejeo, el borracho soltó un brazo y le apretó los genitales.

—No olvides que eres hombre, cagada.

El borracho soltó una carcajada burlona y se puso de pie, cansado, victorioso. Ella solo lloraba.

Mientras el borracho se alejaba de regreso al bar, Elena recogía del piso la ropa nueva, el maquillaje y el DNI: fragmentos de ella misma a los que, otra vez, el Perú les había dicho que no.



