[Atrás](index.html)

<h2>Capricho</h2>

Una larga fila india de camionetas se abría paso por el camino de trocha que llegaba a un asentamiento humano del norte de Piura, buscando al ganador del mayor sorteo de la historia nacional: el señor José. 

Pintadas con los colores de uno de los bancos más grandes del país, las camionetas formaban un tropel festivo que anunciaba su llegada con *Color Esperanza* en sus altavoces, levantando pequeñas cortinas de arena y humo tras de sí. En las tolvas y vestidos uniformemente con polos institucionales, enérgicos grupos de muchachos coreaban la canción con un entusiasmo que no parecía contratado.

Nunca antes se había visto allí tal cantidad de vehículos, mucho menos así de modernos, y mientras la caravana de alegría se adentraba en el vecindario, los niños se asomaban por las ventanas y salían corriendo al encuentro de aquel espectáculo rodante. 

Casi al otro extremo del asentamiento humano, el señor José se encontraba durmiendo sentado justo afuera de su casa, escurrido en la silla y con su sombrero de paja cubriéndole el rostro. Vestía una camisa beige, la cual llevaba abierta y dejaba ver un bivirí agujereado. Llevaba también el pantalón remangado hasta poco antes de las rodillas, y una de sus ojotas negras estaba siendo mordisqueada por el perro de la familia.

Para cuando la caravana llegó a la casa del señor José, luego de hacer múltiples paradas pidiendo indicaciones, éste ya se había despertado de mala gana. La ventana del copiloto de la primera camioneta, un tanto polarizada por el polvo, bajó lentamente y dejó ver a un hombre joven con aires de ejecutivo y de amigable expresión.

—¿José García Sandoval? —preguntó.

El señor José asintió con la cabeza, confundido.

El joven ejecutivo le dirigió unas palabras al chofer y el motor de la camioneta se apagó. Luego bajó de ella y le hizo una señal afirmativa al resto de la caravana. Decenas de personas empezaron a descender de aquella hilera de camionetas, algunas de ellas con cámaras de televisión, otras con sobres manila llenos de papeles por firmar.

La escena confundió aun más al señor José y, percatándose de esto, el joven ejecutivo soltó una inocente carcajada.

—No te alarmes, José —dijo el joven sacudiéndose el saco y, acto seguido, extendió la mano—. Soy el representante oficial del Banco Privado del Perú aquí en Piura.

—Pensaba pagarles la semana que viene —contestó el señor José estrechándole la mano, pero reemplazando la confusión inicial por una precavida hostilidad.

—Soy consciente de la deuda, pero no te preocupes por eso ahora. El motivo de nuestra visita es de una naturaleza mucho más feliz.

Unos metros más atrás, fuera del alcance de aquella conversación, las cámaras de televisión estaban siendo instaladas, las anfitrionas retocaban su maquillaje y un equipo de notarios humedecía sus sellos, preparándose todos para la entrega del gran premio.

—¡Es más! —prosiguió el joven ejecutivo, mientras le dio dos palmaditas a un lado del brazo, aprovechando la intimidad—. Olvídate de la deuda, queda perdonada. Solo hay que mandar unos correitos y hacer unas llamadas, pero yo me encargo.

Más que una genuina muestra de generosidad, lo que el señor José percibió fue una camuflada demostración de poder, y resintió al muchacho por eso.

—Te explico el porqué de nuestra visita. Verás, el mes pasado el banco organizó un sorteo a nivel nacional conmemorando nuestros 120 años de fundación. Queríamos llevar a cabo el sorteo más grande y ambicioso en la historia del país. 

Un ventarrón desértico sacudió fuertemente las calaminas de la casa del señor José, estrépito que interrumpió momentáneamente la conversación y asustó a un par de anfitrionas desprevenidas.

—De los millones de peruanos, ¡tú eres el ganador, José! —continuó explicando el joven representante, con el entusiasmo de un vendedor de autos, pero que no provocó reacción alguna en el señor José—. Aparte de los 120 millones de soles, un millón por cada año desde nuestra fundación, te ofrecemos la gerencia general del banco. Tú y tu familia podrán acceder a una buena calidad de vida, educación y salud.  

En medio del monólogo, hizo una pequeña pausa, recordando un detalle menor.

—No has matado a nadie, ¿no? —preguntó el joven, soltando una risa nerviosa—. No es que importe mucho, pero sí es necesario saberlo. En cualquier caso, nuestro bufete de abogados estará a disposición de tu familia por si necesitan salir de cualquier... aprietito legal.

El señor José seguía sin verse convencido o, al menos, interesado. Un ligero nerviosismo aparecía en el joven ejecutivo: tenía que conseguir para hoy alguien que acepte el premio.

—Y, aquí entre nos —continuó con voz baja—, si decides retirarte de la administración, podemos conseguirte algún ministerio o escaño en el congreso. 

—No me interesa, gracias —respondió ásperamente el señor José—. Soy pobre porque quiero. 

El joven ejecutivo lo miró con una decepción prevista, con la confirmación de un miedo: era la cuarta persona que rechazaba el premio y seguía sin entender por qué. Cuando le asignaron aquella tarea, no se imaginó lo difícil que sería encontrar a alguien que realmente quiera progresar. 

Con la desesperación a flor de piel, trepó rápidamente la tolva de una de las camionetas y, gritando, preguntó a la muchedumbre si alguien quería salir de la pobreza con 120 millones de soles. 

El silencio inicial fue roto cuando una persona, con el rostro indistinguible entre aquel mar de gente empolvada, alzó la voz.

—No sé ustedes, pero a mí me gusta irme a dormir sin poder cenar —dijo anónimamente.

Derrotado, el joven ejecutivo volteó hacia su equipo de trabajo, levantó el brazo e hizo un gesto ágil con la mano. Sentándose nuevamente en el asiento de copiloto, esperó a que desarmaran todo. Subió la ventanilla y en un suspiro largo soltó:

—Gente de mierda.

