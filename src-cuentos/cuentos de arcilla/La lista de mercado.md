[Atrás](index.html)

<h2>La lista de mercado</h2>

Aquel día se cumplían dos meses desde que don Antonio y doña Carmen habían cancelado el viaje al interior del país, interrumpidos por un dolor agudo en el vientre de ella. 

A pesar de verse tentada de ignorar los hincones, don Antonio y sus hijos lograron convencerla de lo contrario, especialmente gracias a la intervención de Lina, su pequeña nieta de cuatro años, quien le pidió que por favor vaya al doctor, recitando casi de memoria un libreto practicado horas antes con su mamá.

Luego de algunos exámenes, la oncóloga había citado para aquel día a don Antonio. Los resultados de la biopsia ya estaban listos y le indicó que venga a recogerlos sin su esposa, que lo mejor para ella era recibir la noticia en un ambiente donde se sienta más tranquila. 

Poco después de las seis de la mañana, Don Antonio se levantó de la cama y, de puntillas para que ella no se despierte, se vistió rápidamente. Salió victorioso del cuarto y pensó en desayunar, pero prefirió hacerlo en la calle para no hacer bulla al hervir el agua o despertarla con el aroma del café recién pasado. A pesar de que la ajetreada vida de un matrimonio con tres hijos había dado paso a la calma rutina de un matrimonio jubilado, despertar temprano seguía siendo parte del quehacer habitual. No había hijos que atender ni trabajo al cual asistir y, aun así, la tetera silbaba todos los días junto a la canción del noticiero de la mañana.

—La fuerza de la costumbre —decía ella. 

Pero por ese día, al menos por ese día, don Antonio quería dejarla descansar hasta tarde.

Antes de salir de casa, don Antonio se detuvo en la sala, en busca del libro de cocina que su esposa guardaba desde su soltería. Buscó una receta específica y escribió la lista de mercado en un papel que guardó en su bolsillo con una sonrisa cálida. Un latigazo de miedo castigó inmediatamente aquella calidez, pero del cual se repuso con un optimismo que, menos mal, aún tenía fuerza. En otro papel dejó escrito que él cocinaría el día de hoy y que desayunaría en la calle, y luego de colocarlo debajo de uno de los imanes de la refrigeradora, cogió sus llaves y salió rumbo al hospital.

Compró un desayuno ligero mientras esperaba en el paradero, abrigado por su chaleco de lana y una camisa manga larga de franela gruesa. Subió al autobús y, una vez sentado, intentó no pensar en los resultados que recibiría, mientras se arrepentía de no haber comprado cualquier periódico. «La doctora sonaba tranquila», pensó don Antonio. Tomó refugio en aquella idea y se distrajo en una discusión política con el señor que estaba sentado a su costado. Encontrar extraños tan dispuestos a conversar era una de las ventajas de los asientos reservados.

Bajó del autobús y caminó hacia el hospital, sumergiéndose en una lenta espiral mental que no le dio más remedio que pensar en los resultados que recibiría. Se propuso no hacer contacto visual con la doctora, con tal de no leer en su rostro lo que decía el papel. Ensayó muchas veces la situación en su cabeza y temía que el nerviosismo o la curiosidad se le deslicen a la fuerza de voluntad.

Para suerte suya, cuando llegó al consultorio, la secretaria lo reconoció y le explicó que la doctora no estaba, pero que le había dejado unos resultados. Buscó en uno de sus cajones y sacó un sobre sellado que tenía el nombre de su esposa. Sin mirarlo, le entregó el sobre con una indiferencia que él por poco se la agradece, y siguió digitando nombres en su computadora.

Sobre en mano, don Antonio salió del consultorio y se sentó en la primera silla que pudo encontrar para, por fin, leer los resultados. Le hubiera gustado postergar más aquel momento, pero sabía también que el peso de aquel sobre en sus manos no le habría dejado caminar. Con las piernas inquietas y las manos aún indecisas, abrió el sobre y leyó los resultados.



Cuando llegó al mercado, la última parada antes de casa, sacó de su bolsillo la lista de mercado que hizo en la mañana. Un poco maltratada por los dobleces, dejaba ver una receta medianamente simple. 

Compró verduras, un sol cincuenta de condimentos, algunos abarrotes en el puesto de una señora que le cayó bien y otros en el puesto de un vecino de la cuadra. 

—Hay que apoyar —decía en voz baja para sí mismo. 

Cuando tocó el turno del pollo, sacó nuevamente la lista del mercado y le leyó las instrucciones al señor que atendía. Eran indicaciones muy específicas que él no lograba comprender pero que el pollero sí, así que esperó con confianza la última bolsa de mercado.

Al regresar, cargó las bolsas como pudo, a veces levantándolas para evitar un par de narices de perro, apurando el paso de vez en cuando en la medida en que la edad se lo permitía. 

Llegó a casa a eso de las diez, justo a tiempo para escuchar su programa de radio favorito, y desde la cocina pudo ver la puerta del dormitorio aún cerrada. Con mucho cuidado puso las verduras sobre la mesa, guardó los abarrotes en la alacena y prendió la radio con el volumen en uno. El susurro de una guitarra melancólica sonaba en su estación favorita.

Abrió el libro de cocina y buscó la página treintaiuno. Alistó los condimentos que iba a usar para sazonar el pollo, se amarró un pequeño mantel a la cintura cual delantal, puso el pollo sobre la mesa y se dio cuenta de que las presas de la bolsa no estaban como en las fotos del libro de cocina.

—Mierda...—soltó—. Me cagaron el pollo.

Y se puso a llorar.

