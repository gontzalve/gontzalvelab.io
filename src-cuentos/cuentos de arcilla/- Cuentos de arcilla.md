<h2>Cuentos de arcilla</h2>

*por Germán Cruz*

----------------------------------------------------

No hay devoluciones de tiempo. 

[Libro de reclamaciones](https://twitter.com/gontzalve_)

------

<h3>Lado A</h3>

[La lista de mercado](La lista de mercado.html)

[Pequeños pasos, pequeñas victorias](Pequeños pasos, pequeñas victorias.html)

[5% (en progreso)](5%.html)

[*Presentes invisibles*](Presentes invisibles.html)

[*Pausa*](Pausa.html)

*A Tatiana*

------

<h3>Lado B</h3>

[Capricho](Capricho.html)

[Recordatorios violentos](Recordatorios violentos.html)